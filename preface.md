# Preface
We are always focused, but we are not always focused upon the right things.

**We are always focused.** Though there are a great many things in existence, we seem to be limited in respect to the number of how many of them we may attend to at any one time. To see the objects in front of you, for instance, is to not see the objects behind you.

In our daily lives we deal with a finer sort of focus. We attend to one object or task despite acknowledging other objects or tasks that we may focus upon.

I suspect that we may see that there is a logic that unifies both of these cases and that our lives may be improved by rendering this logic explicit and useful.